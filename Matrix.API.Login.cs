/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Text.Json.Serialization;
using Microsoft.Extensions.Logging;


namespace Matrix;

partial class API
{
	/// <summary>
	/// Querying available login flows and using one of them to establish an active session for use in other
	/// <see cref="API"/> calls 
	/// </summary>
	public static class Login
	{
		/// <summary>
		/// Bit flags representing supported login flows
		/// </summary>
		[Flags]
		public enum Flow
		{
			/// <summary>
			/// Login not supported
			/// </summary>
			None = 0,
			/// <summary>
			/// Password login supported
			/// </summary>
			Password = 1 << 0,
			/// <summary>
			/// Token login supported
			/// </summary>
			Token = 1 << 1,
			/// <summary>
			/// Token login with get-token supported
			/// </summary>
			GetToken = Token | 1 << 2
		}

		
		// ReSharper disable InconsistentNaming
		private readonly record struct LoginFlow (string type, bool get_login_token);
		private readonly record struct GetFlowsResponseBody (LoginFlow[] flows);
		// ReSharper restore InconsistentNaming


		/// <summary>
		/// Try to query a which login flows a server supports
		/// </summary>
		/// <param name="server">The base URI of the Matrix server to query</param>
		/// <returns>A boolean indicating success and a <see cref="Flow"/> bitmask indicating supported flows</returns>
		public static async Task<(bool success, Flow flows)> TryGetFlowsAsync (Uri server)
		{
			Flow result = Flow.None;
			
			(HTTPResult httpResult, GetFlowsResponseBody flows) =
				await TryHTTPGetAsync<GetFlowsResponseBody> (new Uri (server, kRequestBase + "/login"));

			if (!httpResult.IsSuccess ())
			{
				return (false, result);
			}

			// Handle
			foreach (LoginFlow flow in flows.flows)
			{
				switch (flow.type.ToLowerInvariant ())
				{
					case "m.login.password":
						result |= Flow.Password;
						break;
					case "m.login.token":
						result |= flow.get_login_token ? Flow.GetToken : Flow.Token;
						break;
				}
			}

			return (true, result);
		}
		
		
		// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo
		#pragma warning disable CS0649
		private readonly record struct UserIdentifier (string type, string user);
		private readonly record struct PasswordRequestBody (
			string device_id,
			UserIdentifier identifier,
			string initial_device_display_name,
			string password,
			bool refresh_token,
			string type
		);


		private readonly record struct HomeServerInformation (string base_url);
		private readonly record struct IdentityServerInformation (string base_url);
		private readonly record struct DiscoveryInformation
		{
			[JsonPropertyName ("m.homeserver")] public readonly HomeServerInformation homeserver;
			[JsonPropertyName ("m.identity_server")] public readonly IdentityServerInformation identity_server;
		}
		private readonly record struct PasswordResponseBody (
			string access_token,
			string device_id,
			int expires_in_ms,
			string refresh_token,
			string user_id,
			DiscoveryInformation well_known
		);
		#pragma warning restore CS0649
		// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo


		/// <summary>
		/// Try to log in to a Matrix server using the password flow 
		/// </summary>
		/// <param name="server">The base URI of the Matrix server to query</param>
		/// <param name="username">Name of the user logging in</param>
		/// <param name="password">Password for the specified user</param>
		/// <param name="deviceID">Unique identifier for the current device</param>
		/// <param name="deviceName">Name to use for the current device ID</param>
		/// <param name="retry">Maximum number of retries if rate limited</param>
		/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
		/// <see cref="Matrix.API.DefaultMaxRateLimitDelay"/></param>
		/// <returns>A boolean indicating success and an access token for the session</returns>
		public static async Task<(bool success, string accessToken)> TryPasswordAsync (
			Uri server,
			string username,
			string password,
			string deviceID,
			string deviceName,
			uint retry = 0,
			TimeSpan maxRateLimitDelay = default
		)
		{
			HTTPResult httpResult;
			PasswordResponseBody response;
			
			int retries = 0;
			do
			{
				(httpResult, response) =
					await TryHTTPPostAsync<PasswordRequestBody, PasswordResponseBody> (
						new Uri (server, kRequestBase + "/login"),
						new PasswordRequestBody
						{
							device_id = deviceID,
							identifier = new UserIdentifier (type: "m.id.user", user: username),
							initial_device_display_name = deviceName,
							password = password,
							refresh_token = false,
							type = "m.login.password"
						},
						handleRateLimit: retries < retry,
						maxRateLimitDelay: maxRateLimitDelay
					);

				if (httpResult != HTTPResult.RateLimitWaited)
				{
					break;
				}
				
				Log.LogWarning ("Retrying login");
			} while (++retries <= retry);

			return httpResult.IsSuccess ()
				? (true, response.access_token)
				: (false, "");
		}
	}
}
