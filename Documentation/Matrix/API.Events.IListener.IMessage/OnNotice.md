# API.Events.IListener.IMessage.OnNotice method

Invoked as a room receives a notice

```csharp
public void OnNotice(string room, string id, string sender, string body, string htmlBody)
```

| parameter | description |
| --- | --- |
| room | The room receiving the notice |
| id | Identifier for the event |
| sender | Account which sent the notice |
| body | Raw text body of the notice |
| htmlBody | Optional HTML formatted body of the notice or empty string |

## See Also

* interface [IMessage](../API.Events.IListener.IMessage.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
