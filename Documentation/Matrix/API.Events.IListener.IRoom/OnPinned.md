# API.Events.IListener.IRoom.OnPinned method

Invoked as an event setting the pinned events of a room is received

```csharp
public void OnPinned(string room, string id, string sender, string[] pinnedIDs)
```

| parameter | description |
| --- | --- |
| room | The room receiving the pin list change |
| id | Identifier for the event |
| sender | Account which set the pinned events |
| pinnedIDs | List of IDs for the events now pinned in the room |

## See Also

* interface [IRoom](../API.Events.IListener.IRoom.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
