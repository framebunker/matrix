# API.Events.IListener.IRoom.OnTopic method

Invoked as an event setting the topic of a room is received

```csharp
public void OnTopic(string room, string id, string sender, string topic)
```

| parameter | description |
| --- | --- |
| room | The room receiving the topic change |
| id | Identifier for the event |
| sender | Account which set the topic |
| topic | New room topic |

## See Also

* interface [IRoom](../API.Events.IListener.IRoom.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
