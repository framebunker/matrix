# Session.TryGetImageAsync method

Get the local path to the image referenced by the given ID, downloading it if not yet available

```csharp
public Task<(bool success, string imagePath)> TryGetImageAsync(string imageID)
```

| parameter | description |
| --- | --- |
| imageID | ID of the image - typically received via [`OnImage`](../Session.IListener.IMessage/OnImage.md) |

## Return Value

A boolean indicating success and the local path to the image file

## See Also

* class [Session](../Session.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
