# Session.GetTypists method

Try to get the list of room members currently marked as typing

```csharp
public IEnumerable<string> GetTypists(string roomID)
```

| parameter | description |
| --- | --- |
| roomID | The ID of the room - can be resolved via [`TryGetRoomID`](./TryGetRoomID.md) |

## Return Value

The account IDs of all room members currently marked as typing - empty on unknown !:roomID

## See Also

* class [Session](../Session.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
