# Session.RemoveListener method

Unregister a previously registered receiver of event callbacks

```csharp
public void RemoveListener(IListener listener)
```

| parameter | description |
| --- | --- |
| listener | The instance to no longer receive callbacks |

## See Also

* interface [IListener](../Session.IListener.md)
* class [Session](../Session.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
