# Session.Start method

Log in and start listening for events

```csharp
public Task<bool> Start()
```

## Return Value

Whether the session is running or failed in address resolution or login

## See Also

* class [Session](../Session.md)
* namespace [Matrix](../../Matrix.md)

<!-- DO NOT EDIT: generated by xmldocmd for Matrix.dll -->
