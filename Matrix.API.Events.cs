/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Collections.Specialized;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Web;


namespace Matrix;

partial class API
{
	/// <summary>
	/// Synchronization of room events from the server, passed to listener, and sending out new ones
	/// </summary>
	public static partial class Events
	{
		/// <summary>
		/// Sync event listener base type - direct use only represents the null listener
		/// </summary>
		public partial interface IListener
		{
			/// <summary>
			/// Receiving unprocessed content for all events
			/// </summary>
			public interface IRaw : IListener
			{
				/// <summary>
				/// Invoked as any event is received
				/// </summary>
				/// <param name="room">The room in which the event occured</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="type">Event type</param>
				/// <param name="sender">Account from which the event originated</param>
				/// <param name="content">Json serialized event data</param>
				void OnEvent (string room, string id, string type, string sender, string content);
			}
			
			
			/// <summary>
			/// Receiving events relating to room memberships
			/// </summary>
			public interface IMember : IListener
			{
				/// <summary>
				/// Invoked as an event is received signifying that the sender knocked on the room
				/// </summary>
				/// <param name="room">The room which was knocked on</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which knocked on the room</param>
				/// <param name="displayName">Display name of account which knocked on the room</param>
				void OnKnock (string room, string id, string sender, string displayName);
				/// <summary>
				/// Invoked as an event is received signifying that the sender invited another to join the room
				/// </summary>
				/// <param name="room">The room to which the account was invited</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which sent the invite</param>
				/// <param name="displayName">Display name of account which got invited</param>
				void OnInvite (string room, string id, string sender, string displayName);
				/// <summary>
				/// Invoked as an event is received signifying an account left the room
				/// </summary>
				/// <param name="room">The room which was left</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">The account which left the room</param>
				void OnLeave (string room, string id, string sender);
				/// <summary>
				/// Invoked as an event is received signifying an account joined the room
				/// </summary>
				/// <param name="room">The room which was joined</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">The account which joined the room</param>
				/// <param name="displayName">Display name of the joining account</param>
				/// <param name="avatar">Matrix media mxc protocol URI for the avatar of the joining account</param>
				void OnJoin (string room, string id, string sender, string displayName, Uri? avatar);
				/// <summary>
				/// Invoked as an event is received signifying that the sender banned an account from the room 
				/// </summary>
				/// <param name="room">The room from which the account was banned</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">The account which performed the ban</param>
				/// <param name="displayName">Display name of the account being banned</param>
				/// <param name="reason">The reason given for the ban</param>
				void OnBan (string room, string id, string sender, string displayName, string reason);
			}
			
			
			/// <summary>
			/// Receiving events related to room data and status
			/// </summary>
			public interface IRoom : IListener
			{
				/// <summary>
				/// Invoked as an event setting the name of a room is received
				/// </summary>
				/// <param name="room">The room receiving the name change</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which set the name</param>
				/// <param name="name">New room name</param>
				void OnName (string room, string id, string sender, string name);
				/// <summary>
				/// Invoked as an event setting the topic of a room is received
				/// </summary>
				/// <param name="room">The room receiving the topic change</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which set the topic</param>
				/// <param name="topic">New room topic</param>
				void OnTopic (string room, string id, string sender, string topic);
				/// <summary>
				/// Invoked as an event adding an alias for a room is received
				/// </summary>
				/// <param name="room">The room receiving the alias</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which added the alias</param>
				/// <param name="alias">Additional alias</param>
				void OnAlias (string room, string id, string sender, string alias);
				/// <summary>
				/// Invoked as an event setting the avatar of a room is received
				/// </summary>
				/// <param name="room">The room receiving the avatar change</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which set the avatar</param>
				/// <param name="address">Matrix media mxc protocol URI for the avatar</param>
				void OnAvatar (string room, string id, string sender, Uri? address);
				/// <summary>
				/// Invoked as an event setting the pinned events of a room is received 
				/// </summary>
				/// <param name="room">The room receiving the pin list change</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which set the pinned events</param>
				/// <param name="pinnedIDs">List of IDs for the events now pinned in the room</param>
				void OnPinned (string room, string id, string sender, string[] pinnedIDs);
			}
			
			
			/// <summary>
			/// Receiving events for incoming typing state updates
			/// </summary>
			/// <remarks>Unset typing state prior to sync, then set for all callbacks</remarks>
			public interface ITyping : IListener
			{
				/// <summary>
				/// Invoked at the beginning of a typing state update
				/// </summary>
				/// <remarks>In response to this, all members should be considered not typing until an explicit
				/// <see cref="OnTyping"/> call is received</remarks>
				void OnUpdateBegin ();
				/// <summary>
				/// Invoked as <see cref="sender"/> is marked as typing during this sync
				/// </summary>
				/// <param name="room">The room within which <see cref="sender"/> is typing</param>
				/// <param name="sender">Account marked as typing in the <see cref="room"/></param>
				/// <remarks>Unset typing state prior to sync, then set for all callbacks</remarks> 
				void OnTyping (string room, string sender);
				/// <summary>
				/// Invoked at the end of the typing state update
				/// </summary>
				/// <remarks>Looking at previous typing state versus received <see cref="OnTyping"/> calls during this
				/// update shows which members stopped typing</remarks>
				void OnUpdateEnd ();
			}
			
			
			/// <summary>
			/// Receiving unprocessed content for unhandled events
			/// </summary>
			public interface IUnhandled : IListener
			{
				/// <summary>
				/// Invoked as an event is received and not processed by other handling logic
				/// </summary>
				/// <param name="room">The room in which the event occured</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="type">Event type</param>
				/// <param name="sender">Account from which the event originated</param>
				/// <param name="content">Json serialized event data</param>
				void OnEvent (string room, string id, string type, string sender, string content);
			}
		}


		// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
		#pragma warning disable CS0649
		private readonly record struct ThumbnailInfo (int h, string mimetype, int size, int w);
		private readonly record struct EncryptedFile;
		
		private readonly record struct VideoInfo (
			int duration,
			int h,
			string mimetype,
			int size,
			EncryptedFile thumbnail_file,
			ThumbnailInfo thumbnail_info,
			string thumbnail_url,
			int w
		);
		private readonly record struct LocationInfo (
			EncryptedFile thumbnail_file,
			ThumbnailInfo thumbnail_info,
			string thumbnail_url
		);
		private readonly record struct AudioInfo (
			int duration,
			string mimetype,
			int size
		);
		private readonly record struct FileInfo (
			string mimetype,
			int size,
			EncryptedFile thumbnail_file,
			ThumbnailInfo thumbnail_info,
			string thumbnail_url
		);
		private readonly record struct ImageInfo (
			int h,
			string mimetype,
			int size,
			EncryptedFile thumbnail_file,
			ThumbnailInfo thumbnail_info,
			string thumbnail_url,
			int w
		);
		
		private readonly record struct EventContentMember (
			string? avatar_url,
			string? displayname,
			bool is_direct,
			string? join_authorised_via_users_server,
			string membership,
			string? reason,
			Invite? third_party_invite
		);
		private readonly record struct EventContentRoomName (string name);
		private readonly record struct EventContentRoomTopic (string topic);
		private readonly record struct EventContentRoomAlias (string alias, string[] alt_aliases);
		private readonly record struct EventContentRoomAvatar (ImageInfo info, string url);
		private readonly record struct EventContentRoomPinnedEvents (string[] pinned);
		
		private readonly record struct EventContentTypingUnset (bool typing = false);
		private readonly record struct EventContentTypingSet (int timeout, bool typing = true);
		private readonly record struct EventContentTyping (string[] user_ids);

		private readonly record struct Event (JsonElement content, string type);
		
		private readonly record struct Signed (string mxid, JsonElement signatures, string token);
		private readonly record struct Invite (string display_name, Signed signed);
		private readonly record struct EventContent (
			string avatar_url,
			string displayname,
			bool is_direct,
			string join_authorised_via_users_server,
			string membership,
			string reason,
			Invite third_party_invite
		);
		
		private readonly record struct UnsignedData (
			ulong age,
			EventContent prev_content,
			JsonElement redacted_because, // ClientEventWithoutRoomID - breaking cyclic struct
			string transaction_id
		);
		private readonly record struct ClientEventWithoutRoomID (
			JsonElement content,
			string event_id,
			ulong origin_server_ts,
			string sender,
			string state_key,
			string type,
			UnsignedData unsigned
		);
		
		private readonly record struct AccountData (Event[] events);
		private readonly record struct DeviceLists;
		private readonly record struct OneTimeKeysCount;
		private readonly record struct Presence (Event[] events);

		private readonly record struct StrippedStateEvent (
			EventContent content,
			string sender,
			string state_key,
			string type
		);
		private readonly record struct InviteState (StrippedStateEvent[] events);
		private readonly record struct InvitedRoom (InviteState invite_state);
		
		
		private readonly record struct Ephemeral (Event[] events);
		private readonly record struct State (ClientEventWithoutRoomID[] events);
		private readonly record struct RoomSummary
		{
			[JsonPropertyName ("m.heroes")] public readonly string[] heroes;
			[JsonPropertyName ("m.invited_member_count")] public readonly int invited_member_count;
			[JsonPropertyName ("m.joined_member_count")] public readonly int joined_member_count;
		}
		private readonly record struct Timeline (ClientEventWithoutRoomID[] events, bool limited, string prev_batch);
		private readonly record struct UnreadNotificationCounts (int highlight_count, int notification_count);
		private readonly record struct UnreadThreadNotificationCounts (int highlight_count, int notification_count);
		private readonly record struct JoinedRoom (
			AccountData account_data,
			Ephemeral ephemeral,
			State state,
			RoomSummary summary,
			Timeline timeline,
			UnreadNotificationCounts unread_notifications,
			UnreadThreadNotificationCounts unread_thread_notifications
		);
		
		
		private readonly record struct KnockState (StrippedStateEvent[] events);
		private readonly record struct KnockedRoom (KnockState knock_state);
		
		private readonly record struct LeftRoom (AccountData account_data, State state, Timeline timeline);
		
		private readonly record struct Rooms (
			JsonElement invite, // objects of room name mapping to InvitedRoom
			JsonElement join, // objects of room name mapping to JoinedRoom
			JsonElement knock, // objects of room name mapping to KnockedRoom
			JsonElement leave // objects of room name mapping to LeftRoom
		);
		
		private readonly record struct ToDevice;

		private readonly record struct SyncResponseBody (
			AccountData account_data,
			DeviceLists device_lists,
			OneTimeKeysCount device_one_time_keys_count,
			string next_batch,
			Presence presence,
			Rooms rooms,
			ToDevice to_device
		);
		#pragma warning restore CS0649
		// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
		
		
		private static readonly Dictionary<string, Type> kEventTypeMap = new()
		{
			{ "m.room.message", typeof (EventContentMessage) },
			{ "m.room.member", typeof (EventContentMember) },
			{ "m.room.name", typeof (EventContentRoomName) },
			{ "m.room.topic", typeof (EventContentRoomTopic) },
			{ "m.room.canonical_alias", typeof (EventContentRoomAlias) },
			{ "m.room.avatar", typeof (EventContentRoomAvatar) },
			{ "m.room.pinned_events", typeof (EventContentRoomPinnedEvents) },
			{ "m.typing", typeof (EventContentTyping) },
		};
		
		
#pragma warning disable 1574
		/// <summary>
		/// Try to request an event synchronization batch, processing all returned events supported and listened for
		/// </summary>
		/// <param name="server">The base URI of the Matrix server</param>
		/// <param name="accessToken">Token representing the active session</param>
		/// <param name="listener">Event listener to handle received events</param>
		/// <param name="timeout">Optional setting for how long to wait before returning with all collected events - by
		/// default zero, meaning an immediate return with whatever was available, not waiting for further events</param>
		/// <param name="sinceBatchToken">Token referencing last sync batch, since which events should be loaded - by
		/// default "now", resulting in most recent events being returned</param>
		/// <returns>A boolean indicating success and the token referencing this sync batch</returns>
		/// <remarks><see cref="listener"/> should only implement the <see cref="IListener"/> interfaces for the events
		/// intended to be handled - type checking will result in skipped processing of unhandled events</remarks>
		public static async Task<(bool success, string batchToken)> TrySyncAsync (
			Uri server,
			string accessToken,
			IListener listener,
			TimeSpan timeout = default,
			string? sinceBatchToken = default
		)
		{
			// Build request
			
			Uri request = new (server, kRequestBase + "/sync");

			bool
				hasTimeout = timeout.Ticks > 0,
				hasSince = !string.IsNullOrWhiteSpace (sinceBatchToken!);

			if (hasTimeout || hasSince)
			{
				UriBuilder builder = new (request);
				NameValueCollection query = HttpUtility.ParseQueryString (builder.Query);
				
				if (hasTimeout)
				{
					query["timeout"] = timeout.TotalMilliseconds.ToString (CultureInfo.InvariantCulture);
				}

				if (hasSince)
				{
					query["since"] = sinceBatchToken;
				}
				
				builder.Query = query.ToString ();
				request = builder.Uri;
			}
			
			// Run

			(HTTPResult result, SyncResponseBody response) =
				await TryHTTPGetAsync<SyncResponseBody> (
					request,
					accessToken,
					hasTimeout ? timeout * 2 : DefaultHTTPTimeout
				);

			if (!result.IsSuccess ())
			{
				return (false, sinceBatchToken ?? "");
			}
			
			// Handle
			
			IListener.IRaw? rawListener = listener as IListener.IRaw;
			IListener.IUnhandled? unhandledListener = listener as IListener.IUnhandled;

			IEnumerable<(string, ClientEventWithoutRoomID)>
				stateEvents = response.rooms.join.EnumerateObject<JoinedRoom> ().
					SelectMany (r => r.value.state.events.Select (e => (r.name, e))),
				timelineEvents = response.rooms.join.EnumerateObject<JoinedRoom> ().
					SelectMany (r => r.value.timeline.events.Select (e => (r.name, e)));

			// Client events
			foreach ((string roomID, ClientEventWithoutRoomID current) in stateEvents.Concat (timelineEvents))
			{
				rawListener?.OnEvent (
					room: roomID,
					id: current.event_id,
					type: current.type,
					sender: current.sender,
					content: current.content.GetRawText ()
				);

				if (HandleEvent (roomID, current, listener))
				{
					continue;
				}

				unhandledListener?.OnEvent (
					room: roomID,
					id: current.event_id,
					type: current.type,
					sender: current.sender,
					content: current.content.GetRawText ()
				);
			}
			
			IEnumerable<(string, Event)>
				ephemeralEvents = response.rooms.join.EnumerateObject<JoinedRoom> ().
					SelectMany (r => r.value.ephemeral.events.Select (e => (r.name, e)));

			// Ephemeral events
			foreach ((string roomID, Event current) in ephemeralEvents)
			{
				rawListener?.OnEvent (
					room: roomID,
					id: "",
					type: current.type,
					sender: "",
					content: current.content.GetRawText ()
				);

				if (HandleEvent (roomID, current, listener))
				{
					continue;
				}
				
				unhandledListener?.OnEvent (
					room: roomID,
					id: "",
					type: current.type,
					sender: "",
					content: current.content.GetRawText ()
				);
			}
			
			return (true, response.next_batch);
		}


		/// <summary>
		/// Handle an incoming basic event
		/// </summary>
		/// <param name="roomID">The room in which the event was received</param>
		/// <param name="basicEvent">Event data</param>
		/// <param name="listener">Event listener - the concrete type of which informs processing to perform on the
		/// data before passing to it</param>
		/// <returns>Whether the event was handled by the <see cref="listener"/></returns>
		private static bool HandleEvent (string roomID, Event basicEvent, IListener listener)
		{
			// Pull base event data
			
			basicEvent.Deconstruct (
				type: out string type,
				content: out JsonElement content
			);
			
			// Determine listeners and consequently which event types to skip handling (and deserialization) of

			IListener.ITyping? typingListener = listener as IListener.ITyping;
			bool skipTyping = null == typingListener;

			bool skipAny = skipTyping;

			IEnumerable<Type>? skipped = null;
			if (skipAny)
			{
				skipped = Enumerable.Empty<Type> ();
				skipped = skipTyping ? skipped.Append (typeof (EventContentTyping)) : skipped;
			}
			
			// Deserialize

			if (!content.TryDeserialize (out object? contentData, type.ToLowerInvariant (), kEventTypeMap, skipped))
			{
				return false;
			}
			
			// Handle result

			switch (contentData)
			{
				case EventContentTyping typing:
					typingListener!.OnUpdateBegin ();
					foreach (string userID in typing.user_ids)
					{
						typingListener!.OnTyping (roomID, userID);
					}
					typingListener!.OnUpdateEnd ();
					return true;
				default:
					return false;
			}
		}


		/// <summary>
		/// Handle an incoming client event
		/// </summary>
		/// <param name="roomID">The room in which the event was received</param>
		/// <param name="clientEvent">Event data</param>
		/// <param name="listener">Event listener - the concrete type of which informs processing to perform on the
		/// data before passing to it</param>
		/// <returns>Whether the event was handled by the <see cref="listener"/></returns>
		private static bool HandleEvent (string roomID, ClientEventWithoutRoomID clientEvent, IListener listener)
		{
			// Pull base event data
			
			clientEvent.Deconstruct (
				event_id: out string id,
				type: out string type,
				sender: out string sender,
				content: out JsonElement content,
				origin_server_ts: out _,
				state_key: out _,
				unsigned: out _
			);
			
			// Determine listeners and consequently which event types to skip handling (and deserialization) of
			
			IListener.IMessage? messageListener = listener as IListener.IMessage;
			bool skipMessages = null == messageListener;
			IListener.IMember? memberListener = listener as IListener.IMember;
			bool skipMembers = null == memberListener;
			IListener.IRoom? roomListener = listener as IListener.IRoom;
			bool skipRoom = null == roomListener;
			
			bool skipAny = skipMessages || skipMembers || skipRoom;
			
			IEnumerable<Type>? skipped = null;
			if (skipAny)
			{
				skipped = Enumerable.Empty<Type> ();
				skipped = skipMessages ? skipped.Append (typeof (EventContentMessage)) : skipped;
				skipped = skipMembers ? skipped.Append (typeof (EventContentMember)) : skipped;
				skipped = skipRoom
					? skipped.
						Append (typeof (EventContentRoomName)).
						Append (typeof (EventContentRoomTopic)).
						Append (typeof (EventContentRoomAlias)).
						Append (typeof (EventContentRoomAvatar)).
						Append (typeof (EventContentRoomPinnedEvents))
					: skipped;
			}
			
			// Deserialize

			if (!content.TryDeserialize (out object? contentData, type.ToLowerInvariant (), kEventTypeMap, skipped))
			{
				return false;
			}
			
			// Handle result
			
			switch (contentData)
			{
				case EventContentMessage messageContent:
					return HandleMessage
					(
						id: id,
						roomID: roomID,
						sender: sender,
						messageType: messageContent.msgtype?.ToLowerInvariant () ?? "",
						content: content,
						listener: messageListener!
					);
				case EventContentMember memberContent:
					memberContent.Deconstruct
					(
						membership: out string eventType,
						displayname: out string? displayMame,
						avatar_url: out string? avatarURL,
						reason: out string? reason,
						is_direct: out _,
						join_authorised_via_users_server: out _,
						third_party_invite: out _
					);
					
					displayMame ??= "";
					avatarURL ??= "";
					reason ??= "";

					switch (eventType.ToLowerInvariant ())
					{
						case "knock":
							memberListener!.OnKnock (roomID, id, sender, displayMame);
							return true;
						case "invite":
							memberListener!.OnInvite (roomID, id, sender, displayMame);
							return true;
						case "join":
							memberListener!.OnJoin (
								roomID,
								id,
								sender,
								displayMame,
								string.IsNullOrWhiteSpace (avatarURL) ? null : new Uri (avatarURL)
							);
							return true;
						case "leave":
							memberListener!.OnLeave (roomID, id, sender);
							return true;
						case "ban":
							memberListener!.OnBan (roomID, id, sender, displayMame, reason);
							return true;
						default:
							return false;
					}
				case EventContentRoomName roomNameContent:
					roomListener!.OnName (roomID, id, sender, roomNameContent.name);
					return true;
				case EventContentRoomTopic roomTopicContent:
					roomListener!.OnTopic (roomID, id, sender, roomTopicContent.topic);
					return true;
				case EventContentRoomAlias roomAliasContent:
					roomAliasContent.Deconstruct (
						alias: out string alias,
						alt_aliases: out string[] alternatives
					);

					roomListener!.OnAlias (
						roomID,
						id,
						sender,
						string.IsNullOrWhiteSpace (alias) ? alternatives.FirstOrDefault () ?? "" : alias
					);
					return true;
				case EventContentRoomAvatar roomAvatarContent:
					roomAvatarContent.Deconstruct (
						url: out string url,
						info: out _
					);
					
					roomListener!.OnAvatar (
						roomID,
						id,
						sender,
						string.IsNullOrWhiteSpace (url) ? null : new Uri (url)
					);
					return true;
				case EventContentRoomPinnedEvents pinnedEventsContent:
					roomListener!.OnPinned (roomID, id, sender, pinnedEventsContent.pinned);
					return true;
				default:
					return false;
			}
		}
#pragma warning restore 1574
	}
}
