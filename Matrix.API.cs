/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;
using Microsoft.Extensions.Logging;


namespace Matrix;

#pragma warning disable 1574
internal static class Extensions
{
	public static void Message (this ILogger logger, string format, params object[] arguments)
		=> logger.Log (LogLevel.Information, format, arguments);
	
	public static void Error (this ILogger logger, string format, params object[] arguments)
		=> logger.Log (LogLevel.Error, format, arguments);
	
	
	/// <summary>
	/// Enumerate identical properties of a json object as name-<see cref="TValue"/> tuples
	/// </summary>
	/// <param name="element">The json object</param>
	/// <typeparam name="TValue">Type each child object of <see cref="element"/> should be deserialized as</typeparam>
	/// <returns>Enumerable of string-<see cref="TValue"/> pairs</returns>
	/// <remarks>Properties not deserializing as <see cref="TValue"/> are ignored</remarks>
	public static IEnumerable<(string name, TValue value)> EnumerateObject<TValue> (this JsonElement element)
	where TValue : struct
	{
		if (JsonValueKind.Object != element.ValueKind)
		{
			yield break;
		}

		foreach (JsonProperty property in element.EnumerateObject ())
		{
			TValue value;
				
			try
			{
				value = property.Value.Deserialize<TValue> ();
			}
			catch (Exception e)
			{
				API.HandleSerializationException (e);
				continue;
			}

			yield return (property.Name, value);
		}
	}
	
	
	/// <summary>
	/// Try to serialize given <see cref="data"/> to the <see cref="Stream"/> as json
	/// </summary>
	/// <param name="destination">Where to send resulting json data</param>
	/// <param name="data">Data to serialize</param>
	/// <returns>Boolean indicating success</returns>
	public static async Task<bool> TrySerializeAsync<TSerialized> (this Stream destination, TSerialized data)
	where TSerialized : struct
	{
		try
		{
			await JsonSerializer.SerializeAsync (destination, data);
			return true;
		}
		catch (Exception e)
		{
			API.HandleSerializationException (e);
			return false;
		}
	}


	/// <summary>
	/// Try to deserialize a <see cref="TSerialized"/> value from the <see cref="Stream"/> as json
	/// </summary>
	/// <param name="source">Where to read the json from</param>
	/// <returns>A boolean indicating success and, if successful, the deserialized value</returns>
	public static async Task<(bool success, TSerialized result)> TryDeserializeAsync<TSerialized> (this Stream source)
	where TSerialized : struct
	{
		try
		{
			return (true, await JsonSerializer.DeserializeAsync<TSerialized> (source));
		}
		catch (Exception e)
		{
			API.HandleSerializationException (e);
			return (false, default);
		}
	}
	
	
	/// <summary>
	/// Try to deserialize a <see cref="JsonElement"/> as <see cref="TSerialized"/>
	/// </summary>
	/// <param name="element">Serialized data</param>
	/// <param name="result">Deserialized result - if successful</param>
	/// <returns>A boolean indicating success</returns>
	public static bool TryDeserialize<TSerialized> (this JsonElement element, out TSerialized result)
	where TSerialized : struct
	{
		try
		{
			result = element.Deserialize<TSerialized> ();
			return true;
		}
		catch (Exception e)
		{
			API.HandleSerializationException (e);
			result = default;
			return false;
		}
	}
	
	
	/// <summary>
	/// Try to deserialize a <see cref="JsonElement"/> as a type indicated by <see cref="typeString"/> and resolved by
	/// <see cref="typeStringMap"/>
	/// </summary>
	/// <param name="element">Serialized data</param>
	/// <param name="result">Deserialized result - if successful</param>
	/// <param name="typeString">String indicating the type to deserialize as</param>
	/// <param name="typeStringMap">Dictionary mapping type strings to actual types for deserialization</param>
	/// <param name="skip">Optional resolved types to skip</param>
	/// <returns>A boolean indicating success</returns>
	public static bool TryDeserialize (
		this JsonElement element,
		out object? result,
		string typeString,
		IDictionary<string, Type> typeStringMap,
		IEnumerable<Type>? skip = null
	)
	{
		if (!typeStringMap.TryGetValue (typeString, out Type type) || (null != skip && skip.Contains (type)))
		{
			result = default;
			return false;
		}
		
		try
		{
			result = element.Deserialize (type!);
			return true;
		}
		catch (Exception e)
		{
			API.HandleSerializationException (e);
			result = default;
			return false;
		}
	}
}


/// <summary>
/// Stateless, low-allocation, exception-free implementation of the Matrix client-server API
/// </summary>
public static partial class API
{
	// https://spec.matrix.org/v1.8/client-server-api/
	private const string kRequestBase = "_matrix/client/v3";

	private const string
		kHTTPRequestTaskInvalidMessage = "Provided request function returned null task.",
		kHTTPRequestBodyInvalidMessage = "Provided request body failed to serialize.";


	internal static void HandleSerializationException (Exception e) => Log.Error ("Serialization exception:\n" + e);
	private static void HandleHTTPException (Exception e) => Log.Error ("HTTP exception:\n" + e);


	/// <summary>
	/// Logging configuration for both <see cref="API"/> and <see cref="Session"/>
	/// </summary>
	public static class Logger
	{
		private static ILoggerFactory s_Factory = LoggerFactory.Create (builder => builder.AddConsole ());
		
		
		/// <summary>
		/// Replace default logger factory (default settings, piped to console)
		/// </summary>
		/// <param name="factory">The <see cref="ILoggerFactory"/> to use</param>
		public static void SetFactory (ILoggerFactory factory) => s_Factory = factory;
		
		
		internal static ILogger Create (string category) => s_Factory.CreateLogger (category);
	}


	private static ILogger? s_Log;
	private static ILogger Log => s_Log ??= Logger.Create (nameof (API));
	
	
	private enum HTTPResult
	{
		Success,
		InvalidRequestTask,
		InvalidRequestBody,
		Exception,
		Cancellation,
		ErrorCode,
		RateLimitWaited,
		Unparsed
	}


	private static bool IsSuccess (this HTTPResult result) => HTTPResult.Success == result;
	private static bool IsInvalid (this HTTPResult result)
		=> result switch
		{
			HTTPResult.InvalidRequestTask => true,
			HTTPResult.InvalidRequestBody => true,
			_ => false
		};
	private static bool IsError (this HTTPResult result) => !result.IsSuccess () && !result.IsInvalid ();
	

	private enum HTTPMethod
	{
		Post,
		Put
	}
	
	
	// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
	#pragma warning disable CS0649
	private readonly record struct RateLimitError (string errcode, string error, int retry_after_ms);
	#pragma warning restore CS0649
	// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local


	/// <summary>
	/// The timeout used when no explicit timeout is given for a request, default 50 seconds
	/// </summary>
	private static TimeSpan DefaultHTTPTimeout { get; set; } = TimeSpan.FromSeconds (50f);
	
	
	/// <summary>
	/// The max rate limit delay used when no explicit max is given for a request, default 50 seconds
	/// </summary>
	private static TimeSpan DefaultMaxRateLimitDelay { get; set; } = TimeSpan.FromSeconds (50f);


	/// <summary>
	/// Get a <see cref="HttpClient"/> instance
	/// </summary>
	/// <param name="accessToken">Optionally configure the client with an access token authentication header</param>
	/// <param name="timeout">Optionally configure the client with a timeout to use in place of the default</param>
	/// <returns>A client instance for the requester to dispose</returns>
	/// <remarks>Remember to dispose the received instance</remarks> 
	private static HttpClient GetHTTPClient (string? accessToken, TimeSpan timeout = default)
	{
		HttpClient client = new ();
		
		if (!string.IsNullOrWhiteSpace (accessToken!))
		{
			client.DefaultRequestHeaders!.Authorization = new AuthenticationHeaderValue ("Bearer", accessToken);
		}
		
		client.Timeout = timeout.Ticks > 0 ? timeout : DefaultHTTPTimeout;

		return client;
	}


	/// <summary>
	/// Try running a HTTP request
	/// </summary>
	/// <param name="request">The request to run</param>
	/// <param name="accessToken">Optionally configure the client with an access token authentication header</param>
	/// <param name="timeout">Optionally configure the client with a timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success and a request response message for the requester to
	/// dispose</returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult result, HttpResponseMessage responseMessage)>
		TryHTTPRequestAsync (
			Func<HttpClient, Task<HttpResponseMessage>> request,
			string? accessToken,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	{
		using HttpClient client = GetHTTPClient (accessToken, timeout);
		try
		{
			Task<HttpResponseMessage>? task = request (client);

			if (null == task)
			{
				return (HTTPResult.InvalidRequestTask, new HttpResponseMessage { ReasonPhrase = kHTTPRequestTaskInvalidMessage });
			}
			
			HttpResponseMessage responseMessage = await task;
			HTTPResult result = responseMessage.IsSuccessStatusCode ? HTTPResult.Success : HTTPResult.ErrorCode; 

			if (handleRateLimit && await RateLimitDelay (responseMessage, maxRateLimitDelay))
			{
				result = HTTPResult.RateLimitWaited;
			}

			return (result, responseMessage);
		}
		catch (Exception e)
		{
			HTTPResult result = HTTPResult.Exception;
			
			if (e is TaskCanceledException ce)
			{
				result = HTTPResult.Cancellation;
				
				Log.LogWarning ("HTTP request canceled: " + ce.Message);
			}
			else
			{
				HandleHTTPException (e);
			}

			return (result, new HttpResponseMessage { ReasonPhrase = e.Message });
		}
	}


	/// <summary>
	/// Apply any rate limit delay instructed in the given <see cref="response"/> or return immediately
	/// </summary>
	/// <param name="response">The response potentially instructing a rate limit delay</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>Whether rate limit delay was applied</returns>
	private static async Task<bool> RateLimitDelay (HttpResponseMessage response, TimeSpan maxRateLimitDelay = default)
	{
		if (HttpStatusCode.TooManyRequests != response.StatusCode)
		{
			return false;
		}
		
		(bool parseSuccess, RateLimitError rateLimitError) = await (await response.Content!.ReadAsStreamAsync ()).
			TryDeserializeAsync<RateLimitError> ();

		if (!parseSuccess)
		{
			return false;
		}

		maxRateLimitDelay = maxRateLimitDelay.Ticks < 1 ? DefaultMaxRateLimitDelay : maxRateLimitDelay;
		TimeSpan rateLimitDelay = TimeSpan.FromMilliseconds (rateLimitError.retry_after_ms);
		if (rateLimitDelay > maxRateLimitDelay)
		{
			rateLimitDelay = maxRateLimitDelay;
		}

		Log.LogWarning ("Rate limit: Waiting for {0}", rateLimitDelay);

		await Task.Delay (rateLimitDelay);

		return true;
	}


	/// <summary>
	/// Try parting the response from a HTTP request
	/// </summary>
	/// <param name="result">The result from the request</param>
	/// <param name="response">Response message from the request</param>
	/// <typeparam name="TResponse">Type to parse the response body into</typeparam>
	/// <returns>A <see cref="HTTPResult"/> indicating success, the response message, and a response body</returns>
	/// <remarks>Remember to dispose the response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody)>
		TryParseResponseAsync<TResponse> (HTTPResult result, HttpResponseMessage response)
		where TResponse : struct
	{
		// Request error 
		if (HTTPResult.Success != result)
		{
			return (result, response, default);
		}

		// Parse
		(bool parseSuccess, TResponse body) = await (await response.Content!.ReadAsStreamAsync ()).
			TryDeserializeAsync<TResponse> ();
		
		return
			parseSuccess
				? (HTTPResult.Success, response, body)
				: (HTTPResult.Unparsed, response, default);
	}


	/// <summary>
	/// Try running an async HTTP GET request, returning the received <see cref="HttpResponseMessage"/>
	/// </summary>
	/// <param name="request">The GET request to try</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success and a request response message for the requester to
	/// dispose</returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage)>
		TryHTTPGetAsync (
			Uri request,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	{
		(HTTPResult result, HttpResponseMessage response) =
			await TryHTTPRequestAsync (
				async c => await c.GetAsync (request)!,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);
		
		return (result, response);
	}


	/// <summary>
	/// Try running an async HTTP GET request, returning the received <see cref="HttpResponseMessage"/> and, on success,
	/// the response body deserialized as json to a struct of the given <see cref="TResponse"/> type
	/// </summary>
	/// <param name="request">The GET request to try</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success, a request response for the requester to dispose, and a
	/// response body</returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody)>
		TryHTTPGetFullAsync<TResponse> (
			Uri request,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TResponse : struct
	{
		(HTTPResult result, HttpResponseMessage response) =
			await TryHTTPGetAsync (request, accessToken, timeout, handleRateLimit, maxRateLimitDelay);
		
		return await TryParseResponseAsync<TResponse> (result, response);
	}


	/// <summary>
	/// Try running an async HTTP GET request, on success returning the response body deserialized as json to a struct 
	/// of the given <see cref="TResponse"/> type
	/// </summary>
	/// <param name="request">The GET request to try</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success and a response body</returns>
	private static async Task<(HTTPResult success, TResponse responseBody)>
		TryHTTPGetAsync<TResponse> (
			Uri request,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TResponse : struct
	{
		(HTTPResult result, HttpResponseMessage responseMessage, TResponse responseBody) =
			await TryHTTPGetFullAsync<TResponse> (request, accessToken, timeout, handleRateLimit, maxRateLimitDelay);
		
		responseMessage.Dispose ();

		return (result, responseBody);
	}
	
	
	/// <summary>
	/// Try running an async HTTP PUT or POST request, returning the received <see cref="HttpResponseMessage"/>
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="method">Whether the request should be PUT or POST</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success and a request response for the requester to dispose
	/// </returns>
	/// <exception cref="ArgumentOutOfRangeException">If provided an unhandled <see cref="method"/> value</exception>
	/// <remarks>Remember to dispose the received response message</remarks> 
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage)>
		TryHTTPFullAsync<TRequest> (
			Uri request,
			HTTPMethod method,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	{
		// Serialize
		using MemoryStream stream = new ();
		if (!await stream.TrySerializeAsync (requestBody))
		{
			return (
				HTTPResult.InvalidRequestBody,
				new HttpResponseMessage { ReasonPhrase = kHTTPRequestBodyInvalidMessage }
			);
		}

		// Run
		stream.Seek (0, SeekOrigin.Begin);
		using StreamContent content = new (stream);
		
		(HTTPResult result, HttpResponseMessage response) = await TryHTTPRequestAsync (
			method switch
			{
				// ReSharper disable once AccessToDisposedClosure
				HTTPMethod.Post => async c => await c.PostAsync (request, content)!,
				// ReSharper disable once AccessToDisposedClosure
				HTTPMethod.Put => async c => await c.PutAsync (request, content)!,
				_ => throw new ArgumentOutOfRangeException (nameof (method))
			},
			accessToken,
			timeout,
			handleRateLimit,
			maxRateLimitDelay
		);

		return (result, response);
	}


	/// <summary>
	/// Try running an async HTTP PUT or POST request, returning the received <see cref="HttpResponseMessage"/> and, on
	/// success, the response body deserialized as json to a struct of the given <see cref="TResponse"/> type
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="method">Whether the request should be PUT or POST</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success, a request response for the requester to dispose, and a
	/// response body</returns>
	/// <exception cref="ArgumentOutOfRangeException">If provided an unhandled <see cref="method"/> value</exception>
	/// <remarks>Remember to dispose the received response message</remarks> 
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody)>
		TryHTTPFullAsync<TRequest, TResponse> (
			Uri request,
			HTTPMethod method,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	where TResponse : struct
	{
		(HTTPResult result, HttpResponseMessage response) =
			await TryHTTPFullAsync (
				request,
				method,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);
		
		return await TryParseResponseAsync<TResponse> (result, response);
	}


	/// <summary>
	/// Try running an async HTTP POST request, returning the received <see cref="HttpResponseMessage"/> and, on
	/// success, the response body deserialized as json to a struct of the given <see cref="TResponse"/> type 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success, a request response for the requester to dispose, and a
	/// response body</returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody)>
		TryHTTPPostFullAsync<TRequest, TResponse> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	where TResponse : struct
	{
		(HTTPResult result, HttpResponseMessage responseMessage, TResponse responseBody) =
			await TryHTTPFullAsync<TRequest, TResponse> (
				request,
				HTTPMethod.Post,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);

		return (result, responseMessage, responseBody);
	}


	/// <summary>
	/// Try running an async HTTP POST request, on success returning the response body deserialized as json to a struct
	/// of the given <see cref="TResponse"/> type 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success and a response body</returns>
	private static async Task<(HTTPResult success, TResponse responseBody)>
		TryHTTPPostAsync<TRequest, TResponse> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	where TResponse : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody) =
			await TryHTTPPostFullAsync<TRequest, TResponse> (
				request,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);
		
		responseMessage.Dispose ();

		return (success, responseBody);
	}
	
	
	/// <summary>
	/// Try running an async HTTP POST request, returning the received <see cref="HttpResponseMessage"/> 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success and a request response for the requester to dispose
	/// </returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage)>
		TryHTTPPostFullAsync<TRequest> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	{
		(HTTPResult result, HttpResponseMessage responseMessage) =
			await TryHTTPFullAsync (
				request,
				HTTPMethod.Post,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);

		return (result, responseMessage);
	}


	/// <summary>
	/// Try running an async HTTP POST request 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success</returns>
	private static async Task<HTTPResult>
		TryHTTPPostAsync<TRequest> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage) =
			await TryHTTPPostFullAsync (request, requestBody, accessToken, timeout, handleRateLimit, maxRateLimitDelay);
		
		responseMessage.Dispose ();

		return success;
	}


	/// <summary>
	/// Try running an async HTTP PUT request, returning the received <see cref="HttpResponseMessage"/> and, on
	/// success, the response body deserialized as json to a struct of the given <see cref="TResponse"/> type 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success, a request response for the requester to dispose, and a
	/// response body</returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody)>
		TryHTTPPutFullAsync<TRequest, TResponse> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	where TResponse : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody) =
			await TryHTTPFullAsync<TRequest, TResponse> (
				request,
				HTTPMethod.Put,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);

		return (success, responseMessage, responseBody);
	}


	/// <summary>
	/// Try running an async HTTP PUT request, on success returning the response body deserialized as json to a struct
	/// of the given <see cref="TResponse"/> type 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param>
	/// <returns>A <see cref="HTTPResult"/> indicating success and a response body</returns>
	private static async Task<(HTTPResult success, TResponse responseBody)>
		TryHTTPPutAsync<TRequest, TResponse> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	where TResponse : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage, TResponse responseBody) =
			await TryHTTPPutFullAsync<TRequest, TResponse> (
				request,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);
		
		responseMessage.Dispose ();

		return (success, responseBody);
	}
	
	
	/// <summary>
	/// Try running an async HTTP PUT request, returning the received <see cref="HttpResponseMessage"/> 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success and a request response for the requester to dispose
	/// </returns>
	/// <remarks>Remember to dispose the received response message</remarks>
	private static async Task<(HTTPResult success, HttpResponseMessage responseMessage)>
		TryHTTPPutFullAsync<TRequest> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage) =
			await TryHTTPFullAsync (
				request,
				HTTPMethod.Put,
				requestBody,
				accessToken,
				timeout,
				handleRateLimit,
				maxRateLimitDelay
			);

		return (success, responseMessage);
	}


	/// <summary>
	/// Try running an async HTTP PUT request 
	/// </summary>
	/// <param name="request">The request to try</param>
	/// <param name="requestBody">Data to be serialized to json and provided as the body of the request</param>
	/// <param name="accessToken">Optionally run request with an access token authentication header</param>
	/// <param name="timeout">Optional explicit timeout to use in place of the default</param>
	/// <param name="handleRateLimit">Whether to perform any received rate limiting wait</param>
	/// <param name="maxRateLimitDelay">Alternative maximum delay for rate limit wait - default is
	/// <see cref="DefaultMaxRateLimitDelay"/></param> 
	/// <returns>A <see cref="HTTPResult"/> indicating success</returns>
	private static async Task<HTTPResult>
		TryHTTPPutAsync<TRequest> (
			Uri request,
			TRequest requestBody,
			string? accessToken = null,
			TimeSpan timeout = default,
			bool handleRateLimit = false,
			TimeSpan maxRateLimitDelay = default
		)
	where TRequest : struct
	{
		(HTTPResult success, HttpResponseMessage responseMessage) =
			await TryHTTPPutFullAsync (request, requestBody, accessToken, timeout, handleRateLimit, maxRateLimitDelay);
		
		responseMessage.Dispose ();

		return success;
	}
}
#pragma warning restore 1574
