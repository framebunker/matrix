/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using Microsoft.Extensions.Logging;


namespace Matrix;

#pragma warning disable 1574
/// <summary>
/// Stateful implementation of a Matrix client-server session via <see cref="API"/>, tracking rooms, members, and images,
/// wrapping and forwarding events, handling media download, etc.
/// </summary>
public class Session : IDisposable,
	API.Events.IListener.IUnhandled,
	API.Events.IListener.IMessage,
	API.Events.IListener.IMember,
	API.Events.IListener.IRoom,
	API.Events.IListener.ITyping
{
	private const string
		kDeviceID = "edea512c-2d01-4f2e-a41e-ccf2e61e2889",
		kDeviceName = "Apoc";
	private const bool kFetchMemberAvatars = false;
	
	
	/// <summary>
	/// Event listener base type - direct use only represents the null listener
	/// </summary>
	public interface IListener
	{
		/// <summary>
		/// Receiving events for incoming messages 
		/// </summary>
		public interface IMessage : IListener
		{
			/// <summary>
			/// Invoked as a room receives a text message
			/// </summary>
			/// <param name="id">Identifier for the event</param>
			/// <param name="room">ID or alias for the <see cref="Room"/> in receiving the message</param>
			/// <param name="sender"><see cref="Member"/> ID of the message sender</param>
			/// <param name="body">Raw text body of the message</param>
			/// <param name="htmlBody">Optional HTML formatted body of the message or empty string</param>
			void OnText (string id, string room, string sender, string body, string htmlBody);
			/// <summary>
			/// Invoked as a room receives an emote
			/// </summary>
			/// <param name="id">Identifier for the event</param>
			/// <param name="room">ID or alias for the <see cref="Room"/> in receiving the emote</param>
			/// <param name="sender"><see cref="Member"/> ID of the emote sender</param>
			/// <param name="body">Raw text body of the emote</param>
			/// <param name="htmlBody">Optional HTML formatted body of the emote or empty string</param>
			void OnEmote (string id, string room, string sender, string body, string htmlBody);
			/// <summary>
			/// Invoked as a room receives a notice
			/// </summary>
			/// <param name="id">Identifier for the event</param>
			/// <param name="room">ID or alias for the <see cref="Room"/> in receiving the notice</param>
			/// <param name="sender"><see cref="Member"/> ID of the notice sender</param>
			/// <param name="body">Raw text body of the notice</param>
			/// <param name="htmlBody">Optional HTML formatted body of the notice or empty string</param>
			void OnNotice (string id, string room, string sender, string body, string htmlBody);
			/// <summary>
			/// Invoked as a room receives an image message
			/// </summary>
			/// <param name="id">Identifier for the event</param>
			/// <param name="room">ID or alias for the <see cref="Room"/> in receiving the image</param>
			/// <param name="sender"><see cref="Member"/> ID of the image sender</param>
			/// <param name="body">Optional raw text body to go along with the image or empty string</param>
			/// <remarks>Used <see cref="id"/> with <see cref="Session.TryGetImageAsync"/> to resolve the image from the
			/// message</remarks>
			void OnImage (string id, string room, string sender, string body);
		}
		
		
		/// <summary>
		/// Receiving events of typing start/stop for members in rooms
		/// </summary>
		public interface ITyping : IListener
		{
			/// <summary>
			/// Sender started typing in the given room
			/// </summary>
			/// <param name="room">ID or alias for the room in which typing started</param>
			/// <param name="sender">Member ID of the typist</param>
			void OnStartTyping (string room, string sender);
			/// <summary>
			/// Sender stopped typing in the given room
			/// </summary>
			/// <param name="room">ID or alias for the room in which typing stopped</param>
			/// <param name="sender">Member ID of the typist</param>
			void OnStopTyping (string room, string sender);
		}
	}
	
	
	private record struct Room (
		string ID,
		string Name,
		string Topic,
		string Alias,
		string Avatar,
		string[] Pinned,
		HashSet<string> Members,
		HashSet<string> Typing,
		HashSet<string> WasTyping
	);
	private record struct Member (string ID, string Name, string Avatar);
	private record struct Image (string ID, Uri Address, string Path);
	
	
	private static ILogger? s_Log;
	private static ILogger Log => s_Log ??= API.Logger.Create (nameof (Session));


	private readonly string m_Username;
	private readonly Func<string> m_GetPassword;
	private readonly CancellationTokenSource m_CancellationTokenSource = new ();

	private readonly HashSet<IListener.IMessage> m_MessageListeners = new ();
	private readonly HashSet<IListener.ITyping> m_TypingListeners = new ();
	private readonly Dictionary<string, Room> m_Rooms = new ();
	private readonly Dictionary<string, Member> m_Members = new ();
	private readonly Dictionary<string, Image> m_Images = new ();
	
	private Uri m_Server;
	private string? m_AccessToken;
	private TaskCompletionSource<bool> m_FirstSyncCompleteSource;
	private int m_LastTransactionID;
	private string
		m_TypingRoom,
		m_TypingRoomAlias;
	DateTime m_LastTypingPing = DateTime.MinValue;
	
	
	/// <summary>
	/// Task completed when, following a successful call to <see cref="Start"/>, the first sync has completed
	/// </summary>
	public Task FirstSyncCompleteTask => m_FirstSyncCompleteSource.Task!;


	/// <summary>
	/// The ID of the user this session is logged in as
	/// </summary>
	public string UserID { get; }


	/// <summary>
	/// All known rooms - represented as ID or alias
	/// </summary>
	public IEnumerable<string> Rooms
	{
		get
		{
			foreach (Room current in m_Rooms.Values)
			{
				string room = current.Alias;
				yield return string.IsNullOrWhiteSpace (room) ? current.ID : room;
			}
		}
	}

	
	/// <summary>
	/// The room in which this session is currently signaling that it is typing
	/// </summary>
	public string TypingRoom
	{
		get => m_TypingRoomAlias;
		set
		{
			if (value == m_TypingRoomAlias)
			{
				return;
			}

			if (!string.IsNullOrEmpty (m_TypingRoom))
			{
				m_LastTypingPing = DateTime.MinValue;
				_ = API.Events.Send.TryTypingUnset (m_Server, m_AccessToken ?? "", m_TypingRoom, UserID);
			}
			
			m_TypingRoom = TryGetRoomID (value, out string roomID) ? roomID : "";
			m_TypingRoomAlias = value;
		}
	}


	/// <summary>
	/// Maximum number of consecutive failed sync calls at which point disconnect is assumed - default 3, set to 0 or 1
	/// to assume disconnect at very first failed sync
	/// </summary>
	public uint MaxFailedSync { get; set; } = 3;


	/// <summary>
	/// Create a new <see cref="Session"/> for <see cref="username"/> connecting to <see cref="server"/>
	/// </summary>
	/// <param name="server">General unresolved Matrix server URL - like http://matrix.org</param>
	/// <param name="username">User name of the account to log in to</param>
	/// <param name="getPassword">Getter returning the password to go with <see cref="username"/></param>
	/// <remarks><see cref="getPassword"/> is only invoked once at login, return value passed straight to
	/// <see cref="API.Login.TryPasswordAsync"/> - it is not stored</remarks>
	public Session (Uri server, string username, Func<string> getPassword)
	{
		m_Server = server;
		m_Username = username;
		UserID = $"@{m_Username}:{m_Server.Host}";
		m_GetPassword = getPassword;
		m_FirstSyncCompleteSource = new TaskCompletionSource<bool> ();
		m_TypingRoom = m_TypingRoomAlias = "";
	}


	/// <summary>
	/// Stop the session, disconnecting, and clean up all temporary downloaded media assets
	/// </summary>
	public void Dispose ()
	{
		Stop ();

		Files.TryDeleteAll (m_Rooms.Values.Select (r => r.Avatar));
		Files.TryDeleteAll (m_Members.Values.Select (m => m.Avatar));
		Files.TryDeleteAll (m_Images.Values.Select (i => i.Path));
	}


	/// <summary>
	/// Register for receiving event callbacks
	/// </summary>
	/// <param name="listener">The instance to receive the callbacks</param>
	public void AddListener (IListener listener)
	{
		if (listener is IListener.IMessage message)
		{
			m_MessageListeners.Add (message);
		}

		if (listener is IListener.ITyping typing)
		{
			m_TypingListeners.Add (typing);
		}
	}
	
	
	/// <summary>
	/// Unregister a previously registered receiver of event callbacks
	/// </summary>
	/// <param name="listener">The instance to no longer receive callbacks</param>
	public void RemoveListener (IListener listener)
	{
		if (listener is IListener.IMessage message)
		{
			m_MessageListeners.Remove (message);
		}
		
		if (listener is IListener.ITyping typing)
		{
			m_TypingListeners.Remove (typing);
		}
	}
	
	
	/// <summary>
	/// Try to resolve a room ID into a room name / alias
	/// </summary>
	/// <param name="identifier">The room identifier</param>
	/// <param name="name">Name corresponding to the given <see cref="identifier"/> or empty string</param>
	/// <returns>Whether the room is known</returns>
	public bool TryGetRoomName (string identifier, out string name)
	{
		if (!TryResolveAliasToRoomID (identifier, out string roomID) || !m_Rooms.TryGetValue (roomID, out Room room))
		{
			name = "";
			return false;
		}

		name = room.Name;
		return true;
	}


	/// <summary>
	/// Try to resolve a room name / alias into a room ID
	/// </summary>
	/// <param name="identifier">The room identifier</param>
	/// <param name="roomID">ID corresponding to the given <see cref="identifier"/> or empty string</param>
	/// <returns>Whether the room is known</returns>
	public bool TryGetRoomID (string identifier, out string roomID)
	{
		if (TryResolveAliasToRoomID (identifier, out roomID))
		{
			return true;
		}

		roomID = "";
		return false;

	}


	/// <summary>
	/// Try resolve a member ID into a display name
	/// </summary>
	/// <param name="identifier">The member identifier</param>
	/// <param name="name">Name corresponding to the given <see cref="identifier"/> or empty string</param>
	/// <returns>Whether the member is known</returns>
	public bool TryGetMemberName (string identifier, out string name)
	{
		if (!m_Members.TryGetValue (identifier, out Member member))
		{
			name = "";
			return false;
		}
		
		name = member.Name;
		return true;
	}


	/// <summary>
	/// Try to get the list of room members currently marked as typing
	/// </summary>
	/// <param name="roomID">The ID of the room - can be resolved via <see cref="TryGetRoomID"/></param>
	/// <returns>The account IDs of all room members currently marked as typing - empty on unknown <see cref="roomID"/>
	/// </returns>
	public IEnumerable<string> GetTypists (string roomID)
		=> m_Rooms.TryGetValue (roomID, out Room room)
			? room.Typing.Union (room.WasTyping)
			: Enumerable.Empty<string> ();


	private Room GetRoom (string id)
	{
		if (!m_Rooms.TryGetValue (id, out Room room))
		{
			m_Rooms[id] = room = new Room (
				ID: id,
				Name: "",
				Topic: "",
				Alias: "",
				Avatar: "",
				Pinned: Array.Empty<string> (),
				Members: new HashSet<string> (),
				Typing: new HashSet<string> (),
				WasTyping: new HashSet<string> ()
			);
		}

		return room;
	}
	
	
	private void UpdateRoom (string id, Func<Room, Room> update) => m_Rooms[id] = update (GetRoom (id));


	private void RemoveRoom (string id)
	{
		if (!m_Rooms.TryGetValue (id, out Room room))
		{
			return;
		}
		
		Files.TryDelete (room.Avatar);
		m_Rooms.Remove (id);
	}


	private bool TryResolveRoomIDToAlias (string id, out string identifier)
	{
		if (!m_Rooms.TryGetValue (id, out Room room))
		{
			identifier = "";
			return false;
		}
		
		identifier = room.Alias;
		identifier = string.IsNullOrWhiteSpace (identifier) ? room.ID : identifier;

		return true;
	}
	
	
	private bool TryResolveAliasToRoomID (string roomIdentifier, out string roomID)
	{
		if (string.IsNullOrWhiteSpace (roomIdentifier))
		{
			roomID = "";
			return false;
		}
		
		foreach (Room room in m_Rooms.Values)
		{
			if (room.Alias.Equals (roomIdentifier, StringComparison.InvariantCultureIgnoreCase))
			{
				roomID = room.ID;
				return true;
			}
		}

		roomID = "";
		return false;
	}


	private Member GetMember (string id)
	{
		if (!m_Members.TryGetValue (id, out Member member))
		{
			m_Members[id] = member = new Member (ID: id, Name: "", Avatar: "");
		}

		return member;
	}


	private void UpdateMember (string id, Func<Member, Member> update) => m_Members[id] = update (GetMember (id));


	/// <summary>
	/// Log in and start listening for events 
	/// </summary>
	/// <returns>Whether the session is running or failed in address resolution or login</returns>
	public async Task<bool> Start ()
	{
		Log.Message ("Logging in");

		(bool valid, Uri? server) = await API.URL.ResolveAsync (m_Server.ToString ()!);

		if (!valid)
		{
			Log.Error ("Unable to find & resolve valid Matrix URL - from '{0}'", m_Server);
			
			return false;
		}

		m_Server = server!;
		
		(valid, API.Login.Flow flow) = await API.Login.TryGetFlowsAsync (m_Server);

		if (valid)
		{
			Log.Message ("Available login flows: {0}", flow);
		}
		else
		{
			Log.Error ("Query on available login flows failed");

			return false;
		}

		(valid, string accessToken) = await API.Login.TryPasswordAsync (
			server: m_Server,
			username: m_Username,
			password: m_GetPassword () ?? "",
			deviceID: kDeviceID,
			deviceName: kDeviceName,
			retry: 3,
			maxRateLimitDelay: TimeSpan.FromMinutes (2f)
		);

		if (valid)
		{
			Log.Message ("Login succeeded");
		}
		else
		{
			Log.Error ("Login failed");

			return false;
		}

		m_AccessToken = accessToken;
		m_LastTransactionID = 0;
		_ = Run (m_CancellationTokenSource.Token);

		return true;
	}
	
	
	private void Stop ()
	{
		m_CancellationTokenSource.Cancel ();
		m_FirstSyncCompleteSource.TrySetCanceled ();
		m_FirstSyncCompleteSource = new TaskCompletionSource<bool> ();
	}


	private string NewTransactionID () => DateTime.UtcNow.Ticks + "" + ++m_LastTransactionID;
	
	
	private async Task Run (CancellationToken cancellationToken)
	{
		Log.Message ("Run begin");
		
		string? lastBatchToken = null;
		TimeSpan
			requestDelay = TimeSpan.FromMilliseconds (500),
			defaultServerSideTimeout = TimeSpan.FromSeconds (3),
			firstServerSideTimeout = TimeSpan.Zero,
			currentServerSideTimeout = firstServerSideTimeout;

		int failedSync = 0;
		bool firstSync = true;

		CancellationTokenSource typingCheckCancellationTokenSource = new ();

		// The typing check should not be affected by sync timing
		_ = RunTypingCheck (typingCheckCancellationTokenSource.Token);
		
		do
		{
			bool valid;
			string batchToken;
			
			try
			{
				// Perform the actual sync, resulting in callbacks
				(valid, batchToken) =
					await API.Events.TrySyncAsync (
						server: m_Server,
						accessToken: m_AccessToken!,
						listener: this,
						timeout: currentServerSideTimeout,
						sinceBatchToken: lastBatchToken
					);
			}
			// This should never happen, so let's plan for it anyway
			catch (Exception e)
			{
				Log.Error (e.Message ?? e.GetType ()!.FullName!);

				throw;
			}

			// On sync success, progress and reset failure count
			if (valid)
			{
				lastBatchToken = batchToken;
				currentServerSideTimeout = defaultServerSideTimeout;
				
				failedSync = 0;

				if (firstSync)
				{
					firstSync = false;
					m_FirstSyncCompleteSource.SetResult (true);
				}
			}
			// On failure, count it up and then bail or do a quick retry 
			else
			{
				Log.Error ("Sync failed");
				if (++failedSync >= MaxFailedSync)
				{
					Log.Error ("Assuming disconnect at {0} failed sync", failedSync);
					break;
				}
				
				currentServerSideTimeout = firstServerSideTimeout;
			}

			await Task.Delay (requestDelay, cancellationToken);
		}
		while (!cancellationToken.IsCancellationRequested);
		
		typingCheckCancellationTokenSource.Cancel ();
		
		Log.Message ("Run end");
	}


	private async Task RunTypingCheck (CancellationToken cancellationToken)
	{
		TimeSpan
			checkDelay = TimeSpan.FromMilliseconds (500),
			retryDelay = TimeSpan.FromSeconds (60);
		int failedPing = 0;
		
		do
		{
			// Only ping if a typing room is set and we did not just ping
			if (
				!string.IsNullOrEmpty (m_TypingRoom) &&
				(DateTime.Now - m_LastTypingPing).TotalSeconds >= API.Events.kTypingPingFrequencySeconds)
			{
				// Try to send the ping
				if (await API.Events.Send.TryTypingPing (m_Server, m_AccessToken!, m_TypingRoom, UserID))
				{
					failedPing = 0;
					m_LastTypingPing = DateTime.Now;
				}
				// If pinging fails, retry after a short delay - unless cancelled, or a long one if repeated
				else
				{
					Log.Error ("Failed to send typing ping to {0} ({1})", m_TypingRoomAlias, m_TypingRoom);

					if (++failedPing >= MaxFailedSync)
					{
						await Task.Delay (retryDelay, cancellationToken);
					}
				}
			}
			
			await Task.Delay (checkDelay, cancellationToken);
		}
		while (!cancellationToken.IsCancellationRequested);
	}


	/// <summary>
	/// Try to join a room
	/// </summary>
	/// <param name="roomIdentifier">Identifier or alias for the room to join</param>
	/// <param name="reason">Optional message to pair with the join request</param>
	/// <returns>Whether the request was sent successfully</returns>
	public async Task<bool> TryJoinAsync (string roomIdentifier, string? reason = default)
	{
		if (string.IsNullOrWhiteSpace (m_AccessToken!) || string.IsNullOrWhiteSpace (roomIdentifier))
		{
			Log.Error ("Unable to join with invalid access token or room identifier");
			return false;
		}

		(bool success, string _) = await API.Room.TryJoinAsync (m_Server, m_AccessToken, roomIdentifier, reason);

		return success;
	}


	/// <summary>
	/// Try to leave a room
	/// </summary>
	/// <param name="roomIdentifier">Alias for the room to leave</param>
	/// <param name="reason">Optional message to pair with the exit</param>
	/// <returns>Whether the request was sent successfully</returns>
	public async Task<bool> TryLeaveAsync (string roomIdentifier, string? reason = default)
	{
		if (string.IsNullOrWhiteSpace (m_AccessToken!))
		{
			Log.Error ("Unable to leave with invalid access token");
			return false;
		}
		
		if (!TryResolveAliasToRoomID (roomIdentifier, out string roomID))
		{
			Log.Error ("Unable to leave with invalid/unknown room identifier: '{0}'", roomIdentifier);
			return false;
		}

		if (!await API.Room.TryLeaveAsync (m_Server, m_AccessToken, roomID, reason))
		{
			return false;
		}

		RemoveRoom (roomID);
		
		return true;
	}


	/// <summary>
	/// Try to post a message to a room
	/// </summary>
	/// <param name="roomIdentifier">Alias for the room to post in</param>
	/// <param name="message">The text message body</param>
	/// <returns>Whether the message was posted successfully</returns>
	public async Task<bool> TrySendAsync (string roomIdentifier, string message)
	{
		if (string.IsNullOrWhiteSpace (m_AccessToken!))
		{
			Log.Error ("Unable to send with invalid access token");
			return false;
		}
		
		if (!TryResolveAliasToRoomID (roomIdentifier, out string roomID))
		{
			Log.Error ("Unable to send with invalid/unknown room identifier: '{0}'", roomIdentifier);
			return false;
		}

		(bool success, string _) =
			await API.Events.Send.TryMessageAsync (m_Server, m_AccessToken, NewTransactionID (), roomID, message);

		return success;
	}

	
	/// <summary>
	/// Try to get the local path to the downloaded room avatar image file 
	/// </summary>
	/// <param name="roomIdentifier">Alias for the room</param>
	/// <param name="avatarPath">The local image file path if available or empty string</param>
	/// <returns>Whether the room alias is known</returns>
	public bool TryGetRoomAvatar (string roomIdentifier, out string avatarPath)
	{
		if (!TryResolveAliasToRoomID (roomIdentifier, out string roomID))
		{
			avatarPath = "";
			return false;
		}

		avatarPath = m_Rooms[roomID].Avatar;
		return true;
	}


	/// <summary>
	/// Get the local path to the image referenced by the given ID, downloading it if not yet available 
	/// </summary>
	/// <param name="imageID">ID of the image - typically received via <see cref="IListener.IMessage.OnImage"/></param>
	/// <returns>A boolean indicating success and the local path to the image file</returns>
	public async Task<(bool success, string imagePath)> TryGetImageAsync (string imageID)
	{
		string path = "";
		
		if (!m_Images.TryGetValue (imageID, out Image image))
		{
			return (false, path);
		}

		if (!string.IsNullOrWhiteSpace (image.Path))
		{
			return (true, image.Path);
		}
		
		if (!await TrySaveMediaAsync (image.Address, Path.GetTempFileName (), p => path = p))
		{
			return (false, path);
		}

		m_Images[imageID] = image with { Path = path };
		return (true, path);
	}


	/// <summary>
	/// Download server-hosted media referenced by Matrix mxc URL protocol, handling received data directly
	/// </summary>
	/// <param name="mxcUrl">The mxc URL referencing the target media</param>
	/// <param name="handleData">Async callback expected to read the received file stream before completing</param>
	/// <returns>Whether the media was successfully downloaded</returns>
	public async Task<bool> TryGetMediaAsync (Uri mxcUrl, Func<Stream, Task> handleData)
	{
		if (string.IsNullOrWhiteSpace (m_AccessToken!))
		{
			Log.Error ("Unable to get media with invalid access token");
			return false;
		}

		if (!mxcUrl.Scheme.Equals ("mxc", StringComparison.InvariantCultureIgnoreCase))
		{
			Log.Error ("Unable to get media from invalid URL - scheme of '{0}' is '{1}'", mxcUrl, mxcUrl.Scheme);
			return false;
		}
		
		return await API.Media.TryGetAsync (m_Server, m_AccessToken, mxcUrl, handleData);
	}


	/// <summary>
	/// Download server-hosted media referenced by Matrix mxc URL protocol, saving received data to specified file path 
	/// </summary>
	/// <param name="mxcUrl">The mxc URL referencing the target media</param>
	/// <param name="destination">Path to which the received file data should be written</param>
	/// <param name="onReceived">Optional callback on successfully completed download, passing <see cref="destination"/>
	/// </param>
	/// <returns>Whether the media was successfully downloaded</returns>
	public async Task<bool> TrySaveMediaAsync (Uri mxcUrl, string destination, Action<string>? onReceived = null)
	{
		FileStream output;

		try
		{
			output = File.OpenWrite (destination);
		}
		catch (Exception e)
		{
			Log.Error (
				"{0}: Unable to open destination file '{1}' for write: {2}",
				nameof (TrySaveMediaAsync),
				destination,
				e
			);

			return false;
		}

		bool success = await TryGetMediaAsync (mxcUrl, async s => await s.CopyToAsync (output)!);
		await output.DisposeAsync ();

		if (success)
		{
			onReceived?.Invoke (destination);
		}
		
		return success;
	}


	private void UpdateAvatar (string current, Uri? address, Action<string> set)
	{
		// Clean up old avatar
		
		if (File.Exists (current))
		{
			try
			{
				File.Delete (current);
			}
			catch
			{
				// Ignore
			}
		}

		set ("");

		// Leave blank if given a null address
		
		if (null == address)
		{
			return;
		}
		
		// Download and set
		
		_ = TrySaveMediaAsync (address, Path.GetTempFileName (), set);
	}


	void API.Events.IListener.ITyping.OnUpdateBegin ()
	{
		// Store current list of typists and then reset it
		foreach (Room room in m_Rooms.Values)
		{
			room.WasTyping.Clear ();
			foreach (string typist in room.Typing)
			{
				room.WasTyping.Add (typist);
			}
			room.Typing.Clear ();
		}
	}
	
	
	void API.Events.IListener.ITyping.OnTyping (string roomID, string sender)
	{
		if (!m_Rooms.TryGetValue (roomID, out Room room))
		{
			return;
		}
	
		// Register sender as typing, bail if this was known
		room.Typing.Add (sender);
		if (room.WasTyping.Contains (sender))
		{
			return;
		}

		// Was not previously typing, notify listeners
		foreach (IListener.ITyping current in m_TypingListeners)
		{
			current.OnStartTyping (room.Alias, sender);
		}
	}


	void API.Events.IListener.ITyping.OnUpdateEnd ()
	{
		// Review typists who are no longer typing and run OnStopTyping callbacks
		foreach (Room room in m_Rooms.Values)
		{
			foreach (string exTypist in room.WasTyping.Where (t => !room.Typing.Contains (t)))
			{
				foreach (IListener.ITyping current in m_TypingListeners)
				{
					current.OnStopTyping (room.Alias, exTypist);
				}
			}
				
			room.WasTyping.Clear ();
		}
	}


	void API.Events.IListener.IMember.OnKnock (string room, string id, string sender, string displayName) {}
	void API.Events.IListener.IMember.OnInvite (string room, string id, string sender, string displayName) {}
	void API.Events.IListener.IMember.OnBan (string room, string id, string sender, string displayName, string reason) {}


	void API.Events.IListener.IMember.OnJoin (string room, string id, string sender, string displayName, Uri? avatar)
	{
		UpdateMember (sender, m => m with { Name = displayName });
		
		// ReSharper disable HeuristicUnreachableCode
		#pragma warning disable CS0162
		if (kFetchMemberAvatars)
		{
			UpdateAvatar (GetMember (sender).Avatar, avatar, p => UpdateMember (sender, r => r with { Avatar = p }));
		}
		#pragma warning restore CS0162
		// ReSharper disable restore HeuristicUnreachableCode

		GetRoom (room).Members.Add (sender);
	}


	void API.Events.IListener.IMember.OnLeave (string room, string id, string sender)
		=> GetRoom (room).Members.Remove (sender);
	
	
	void API.Events.IListener.IRoom.OnName (string room, string id, string sender, string name)
		=> UpdateRoom (room, r => r with { Name = name });


	void API.Events.IListener.IRoom.OnTopic (string room, string id, string sender, string topic)
		=> UpdateRoom (room, r => r with { Topic = topic });
	
	
	void API.Events.IListener.IRoom.OnAlias (string room, string id, string sender, string alias)
		=> UpdateRoom (room, r => r with { Alias = alias });


	void API.Events.IListener.IRoom.OnAvatar (string room, string id, string sender, Uri? address)
		=> UpdateAvatar (GetRoom (room).Avatar, address, p => UpdateRoom (room, r => r with { Avatar = p }));


	void API.Events.IListener.IRoom.OnPinned (string room, string id, string sender, string[] pinnedIDs)
		=> UpdateRoom (room, r => r with { Pinned = pinnedIDs });


	void API.Events.IListener.IMessage.OnText (string room, string id, string sender, string body, string htmlBody)
	{
		if (!TryResolveRoomIDToAlias (room, out string roomIdentifier))
		{
			Log.Error ("Text message event from unknown room '{0}'", room);
			return;
		}
		
		foreach (IListener.IMessage current in m_MessageListeners)
		{
			current.OnText (id, roomIdentifier, sender, body, htmlBody);
		}
	}
	
	
	void API.Events.IListener.IMessage.OnEmote (string room, string id, string sender, string body, string htmlBody)
	{
		if (!TryResolveRoomIDToAlias (room, out string roomIdentifier))
		{
			Log.Error ("Emote event from unknown room '{0}'", room);
			return;
		}
		
		foreach (IListener.IMessage current in m_MessageListeners)
		{
			current.OnEmote (id, roomIdentifier, sender, body, htmlBody);
		}
	}
	
	
	void API.Events.IListener.IMessage.OnNotice (string room, string id, string sender, string body, string htmlBody)
	{
		if (!TryResolveRoomIDToAlias (room, out string roomIdentifier))
		{
			Log.Error ("Notice event from unknown room '{0}'", room);
			return;
		}
		
		foreach (IListener.IMessage current in m_MessageListeners)
		{
			current.OnNotice (id, roomIdentifier, sender, body, htmlBody);
		}
	}


	void API.Events.IListener.IMessage.OnImage (string room, string id, string sender, string body, Uri? address)
	{
		if (!TryResolveRoomIDToAlias (room, out string roomIdentifier))
		{
			Log.Error ("Image event from unknown room '{0}'", room);
			return;
		}
		
		if (null == address)
		{
			return;
		}

		m_Images[id] = new Image (id, address, "");
		
		foreach (IListener.IMessage current in m_MessageListeners)
		{
			current.OnImage (id, roomIdentifier, sender, body);
		}
	}


	void API.Events.IListener.IUnhandled.OnEvent (string room, string id, string type, string sender, string content)
	{
		/*Console.ForegroundColor = ConsoleColor.DarkGray;
		Console.WriteLine ($"({type})");
		Console.ResetColor ();*/
	}
}
#pragma warning restore 1574
