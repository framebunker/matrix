/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Text.Json;


namespace Matrix;

partial class API
{
	partial class Events
	{
		partial interface IListener
		{
			/// <summary>
			/// Receiving events for incoming messages
			/// </summary>
			public interface IMessage : IListener
			{
				/// <summary>
				/// Invoked as a room receives a text message
				/// </summary>
				/// <param name="room">The room receiving the message</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which sent the message</param>
				/// <param name="body">Raw text body of the message</param>
				/// <param name="htmlBody">Optional HTML formatted body of the message or empty string</param>
				void OnText (string room, string id, string sender, string body, string htmlBody);
				/// <summary>
				/// Invoked as a room receives an emote
				/// </summary>
				/// <param name="room">The room receiving the emote</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which sent the emote</param>
				/// <param name="body">Raw text body of the emote</param>
				/// <param name="htmlBody">Optional HTML formatted body of the emote or empty string</param>
				void OnEmote (string room, string id, string sender, string body, string htmlBody);
				/// <summary>
				/// Invoked as a room receives a notice
				/// </summary>
				/// <param name="room">The room receiving the notice</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which sent the notice</param>
				/// <param name="body">Raw text body of the notice</param>
				/// <param name="htmlBody">Optional HTML formatted body of the notice or empty string</param>
				void OnNotice (string room, string id, string sender, string body, string htmlBody);
				/// <summary>
				/// Invoked as a room receives an image message
				/// </summary>
				/// <param name="room">The room receiving the image</param>
				/// <param name="id">Identifier for the event</param>
				/// <param name="sender">Account which sent the image</param>
				/// <param name="body">Optional paw text body to go along with the image or empty string</param>
				/// <param name="address">Optional mxc-protocol URL for the image or null</param>
				void OnImage (string room, string id, string sender, string body, Uri? address);
			}
		}
		
		
		// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
		#pragma warning disable CS0649
		private readonly record struct EventContentMessageVideo (
			string? body,
			EncryptedFile? file,
			VideoInfo? info,
			string? msgtype,
			string? url
		);
		private readonly record struct EventContentMessageLocation (
			string? body,
			string? geo_uri,
			LocationInfo? info,
			string? msgtype
		);
		private readonly record struct EventContentMessageAudio (
			string? body,
			EncryptedFile? file,
			AudioInfo? info,
			string? msgtype,
			string? url
		);
		private readonly record struct EventContentMessageFile (
			string? body,
			EncryptedFile? file,
			string? filename,
			FileInfo? info,
			string? msgtype,
			string? url
		);
		private readonly record struct EventContentMessageImage (
			string? body,
			EncryptedFile? file,
			ImageInfo? info,
			string? msgtype,
			string? url
		);
		private readonly record struct EventContentMessageNotice (
			string? body,
			string? format,
			string? formatted_body,
			string? msgtype
		);
		private readonly record struct EventContentMessageEmote (
			string? body,
			string? format,
			string? formatted_body,
			string? msgtype
		);
		private readonly record struct EventContentMessageText (
			string? body,
			string? format,
			string? formatted_body,
			string? msgtype
		);
		private readonly record struct EventContentMessageUnformattedText ( // Used in sending only
			string? body,
			string? msgtype
		);
		private readonly record struct EventContentMessage (
			string? body,
			string? msgtype
		);
		#pragma warning restore CS0649
		// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local


		private static readonly Dictionary<string, Type> kMessageTypeMap = new()
		{
			{ "m.text", typeof (EventContentMessageText) },
			{ "m.emote", typeof (EventContentMessageEmote) },
			{ "m.notice", typeof (EventContentMessageNotice) },
			{ "m.image", typeof (EventContentMessageImage) },
			
			// NOTE: Keeping these unresolved while not handling them - no reason to needlessly deserialize 
			//{ "m.file", typeof (EventContentMessageFile) },
			//{ "m.audio", typeof (EventContentMessageAudio) },
			//{ "m.location", typeof (EventContentMessageLocation) },
			//{ "m.video", typeof (EventContentMessageVideo) },
		};


		private static bool HandleMessage
		(
			string id,
			string roomID,
			string sender,
			string messageType,
			JsonElement content,
			IListener.IMessage listener
		)
		{
			if (!content.TryDeserialize (out object? contentData, messageType.ToLowerInvariant (), kMessageTypeMap))
			{
				return false;
			}
		
			switch (contentData)
			{
				case EventContentMessageText text:
					listener.OnText (roomID, id, sender, text.body ?? "", GetHTML (content));
					return true;
				case EventContentMessageEmote emote:
					listener.OnEmote (roomID, id, sender, emote.body ?? "", GetHTML (content));
					return true;
				case EventContentMessageNotice notice:
					listener.OnNotice (roomID, id, sender, notice.body ?? "", GetHTML (content));
					return true;
				case EventContentMessageImage image:
					listener.OnImage (
						roomID,
						id,
						sender,
						image.body ?? "",
						string.IsNullOrWhiteSpace (image.url!) ? null : new Uri (image.url)
					);
					return true;
			}

			return false;
		}


		private static string GetHTML<TContent> (TContent content) where TContent : struct
		{
			string? format = null, body = null;

			switch (content)
			{
				case EventContentMessageText text:
					text.Deconstruct (format: out format, formatted_body: out body, body: out _, msgtype: out _);
					break;
				case EventContentMessageEmote emote:
					emote.Deconstruct (format: out format, formatted_body: out body, body: out _, msgtype: out _);
					break;
				case EventContentMessageNotice notice:
					notice.Deconstruct (format: out format, formatted_body: out body, body: out _, msgtype: out _);
					break;
			}
			
			return 
				null != format && format.Equals ("org.matrix.custom.html", StringComparison.InvariantCultureIgnoreCase)
					? body ?? ""
					: "";
		}
	}
}
