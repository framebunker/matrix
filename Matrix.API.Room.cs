/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Collections.Specialized;
using System.Web;


namespace Matrix;

partial class API
{
	/// <summary>
	/// Outgoing room actions - knocking, leaving, joining
	/// </summary>
	public static class Room
	{
		// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
		#pragma warning disable CS0649
		//private readonly record struct ThirdPartySigned;
		
		private readonly record struct KnockRequestBody (string reason);
		private readonly record struct KnockResponseBody (string room_id);
		
		private readonly record struct JoinRequestBody (string reason);//, ThirdPartySigned? third_party_signed);
		private readonly record struct JoinResponseBody (string room_id);
		
		private readonly record struct LeaveRequestBody (string reason);
		private readonly record struct LeaveResponseBody;
		#pragma warning restore CS0649
		// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
		
		
		/// <summary>
		/// Try to knock on a room
		/// </summary>
		/// <param name="server">The base URI of the Matrix server</param>
		/// <param name="accessToken">Token representing the active session</param>
		/// <param name="roomIdentifier">Identifier for the room to knock on</param>
		/// <param name="reason">Optional message to pass along with the knock</param>
		/// <returns>A boolean indicating success and the room ID as resolved by the server</returns>
		public static async Task<(bool success, string roomID)> TryKnockAsync (
			Uri server,
			string accessToken,
			string roomIdentifier,
			string? reason = default
		)
		{
			// Build request
			
			Uri request = new (server, kRequestBase + "/knock/" + Uri.EscapeDataString (roomIdentifier));
			
			UriBuilder builder = new (request);
			
			NameValueCollection query = HttpUtility.ParseQueryString (builder.Query);
			query["server_name"] = server.ToString ();

			builder.Query = query.ToString ();
			request = builder.Uri;
			
			// Run

			(HTTPResult httpResult, KnockResponseBody response) =
				await TryHTTPPostAsync<KnockRequestBody, KnockResponseBody> (
					request,
					new KnockRequestBody (reason: reason ?? ""),
					accessToken
				);
			
			return httpResult.IsSuccess ()
				? (true, response.room_id)
				: (false, "");
		}
		
		
		/// <summary>
		/// Try to join a room
		/// </summary>
		/// <param name="server">The base URI of the Matrix server</param>
		/// <param name="accessToken">Token representing the active session</param>
		/// <param name="roomIdentifier">Identifier for the room to join</param>
		/// <param name="reason">Optional message to pass along with the join</param>
		/// <returns>A boolean indicating success and the room ID as resolved by the server</returns>
		public static async Task<(bool success, string roomID)> TryJoinAsync (
			Uri server,
			string accessToken,
			string roomIdentifier,
			string? reason = default
		)
		{
			// Build request
			
			Uri request = new (server, kRequestBase + "/join/" + Uri.EscapeDataString (roomIdentifier));
			
			UriBuilder builder = new (request);
			
			NameValueCollection query = HttpUtility.ParseQueryString (builder.Query);
			query["server_name"] = server.ToString ();

			builder.Query = query.ToString ();
			request = builder.Uri;

			// Run

			(HTTPResult httpResult, JoinResponseBody response) =
				await TryHTTPPostAsync<JoinRequestBody, JoinResponseBody> (
					request,
					new JoinRequestBody (reason: reason ?? ""),//, third_party_signed: default),
					accessToken
				);

			return httpResult.IsSuccess ()
				? (true, response.room_id)
				: (false, "");
		}


		/// <summary>
		/// Try to leave a room
		/// </summary>
		/// <param name="server">The base URI of the Matrix server</param>
		/// <param name="accessToken">Token representing the active session</param>
		/// <param name="roomIdentifier">Identifier for the room to leave</param>
		/// <param name="reason">Optional message to pass along with the leave</param>
		/// <returns>A boolean indicating success</returns>
		public static async Task<bool> TryLeaveAsync (
			Uri server,
			string accessToken,
			string roomIdentifier,
			string? reason = default
		)
		{
			// Build request

			Uri request = new (server, kRequestBase + "/rooms/" + roomIdentifier + "/leave");
			
			// Run
			(HTTPResult httpResult, LeaveResponseBody _) =
				await TryHTTPPostAsync<LeaveRequestBody, LeaveResponseBody> (
					request,
					new LeaveRequestBody (reason: reason ?? ""),
					accessToken
				);

			return httpResult.IsSuccess ();
		}
	}
}
