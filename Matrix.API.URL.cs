/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Text.Json;
using System.Text.Json.Serialization;


namespace Matrix;

partial class API
{
	/// <summary>
	/// Resolving Matrix server URIs from provided URLs
	/// </summary>
	public static class URL
	{
		// TODO: Figure out why the direct deserialization fails
		
		// ReSharper disable InconsistentNaming
		private readonly record struct ResolveResponseBody (string server)
		{
			[JsonPropertyName ("m.server")] public readonly string server = server;
		}
		// ReSharper restore InconsistentNaming
	
		
		/// <summary>
		/// Resolve Matrix server URI from base URL
		/// </summary>
		/// <param name="url">A public-facing URL such as http://matrix.org</param>
		/// <returns>The URI for the actual Matrix server</returns>
		public static async Task<(bool valid, Uri? url)> ResolveAsync (string url)
		{
			Uri uri = new (url);
			
			// Request
			
			/*(bool success, ResolveResponseBody response) =
				await TryHTTPGetAsync<ResolveResponseBody> (
					new Uri (uri.Scheme + "://" + uri.DnsSafeHost + "/.well-known/matrix/server")
				);*/
			
			// For now falling back to request...
			
			(HTTPResult httpResult, HttpResponseMessage httpResponse) =
				await TryHTTPGetAsync (
					new Uri (uri.Scheme + "://" + uri.DnsSafeHost + "/.well-known/matrix/server")
				);
			
			if (!httpResult.IsSuccess ())
			{
				return (false, null);
			}
			
			// ... and direct deserialization

			ResolveResponseBody response;
			try
			{
				response = new ResolveResponseBody (
					(await JsonDocument.ParseAsync (await httpResponse.Content!.ReadAsStreamAsync ())).
					RootElement.GetProperty ("m.server").GetString () ?? ""
				);
			}
			catch (Exception e)
			{
				HandleSerializationException (e);
				return (false, null);
			}
			finally
			{
				httpResponse.Dispose ();
			}

			string host = response.server;
	
			// No usable result here
			if (string.IsNullOrWhiteSpace (host))
			{
				return (false, null);
			}
			
			// Make sure the returned URL has the proper scheme specified
			
			uri = new Uri (host);
	
			if (
				host.StartsWith ("https://", StringComparison.InvariantCultureIgnoreCase) ||
				host.StartsWith ("http://", StringComparison.InvariantCultureIgnoreCase)
			)
			{
				return (true, uri);
			}
			
			uri = new Uri ("scheme://" + host);
	
			return (true, new Uri ((uri.Port == 80 ? "http://" : "https://") + host));
		}
		
		
		/// <summary>
		/// Iterate over a collection of URLs, trying to resolve them to Matrix server URIs, returning the first success
		/// </summary>
		/// <param name="urls">Possible base Matrix server URLs</param>
		/// <returns>A boolean indicating success and, if successful, the first resolved Matrix URI</returns>
		public static async Task<(bool found, Uri? url)> FindFirstResolvedAsync (IEnumerable<string> urls)
		{
			foreach (string current in urls)
			{
				(bool valid, Uri? result) = await ResolveAsync (current);
	
				if (valid)
				{
					return (valid, result);
				}
			}
	
			return (false, null);
		}
	}
}
