/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

using System.Collections.Specialized;
using System.Globalization;
using System.Web;


namespace Matrix;

partial class API
{
	/// <summary>
	/// Managing media resources stored server-side on the mxc protocol, like avatars, images used in messages, etc. 
	/// </summary>
	public static class Media
	{
		// ReSharper disable once MemberHidesStaticFromOuterClass
		private const string kRequestBase = "_matrix/media/v3";
		
		
		/// <summary>
		/// Try to download a Matrix media asset 
		/// </summary>
		/// <param name="server">The base URI of the Matrix server</param>
		/// <param name="accessToken">Token representing the active session</param>
		/// <param name="mxcAddress">mxc protocol URI referencing the asset to download</param>
		/// <param name="onData">Async callback expected to read the received <see cref="Stream"/> before completing</param>
		/// <param name="timeout">In case of an incomplete upload, the maximum time to wait for data to be available - or
		/// zero, indicating protocol default (20 seconds)</param>
		/// <returns>A boolean indicating success</returns>
		public static async Task<bool> TryGetAsync (
			Uri server,
			string accessToken,
			Uri mxcAddress,
			Func<Stream, Task> onData,
			TimeSpan timeout = default
		)
		{
			// Build request
			
			Uri request = new (server, $"{kRequestBase}/download/{mxcAddress.Host}/{mxcAddress.PathAndQuery.TrimStart ('/')}");

			UriBuilder builder = new (request);
			NameValueCollection query = HttpUtility.ParseQueryString (builder.Query);

			bool hasTimeout = timeout.TotalMilliseconds > 0;
			
			if (hasTimeout)
			{
				query["timeout_ms"] = timeout.TotalMilliseconds.ToString (CultureInfo.InvariantCulture);
			}

			query["allow_redirect"] = "false";
			query["allow_remote"] = "true";

			builder.Query = query.ToString ();
			request = builder.Uri;

			// Run

			(HTTPResult httpResult, HttpResponseMessage response) = await TryHTTPGetAsync (request, accessToken);
			
			// Handle

			if (httpResult.IsSuccess ())
			{
				Stream data = await response.Content!.ReadAsStreamAsync ();
				await onData (data)!;
				await data.DisposeAsync ();
			}

			response.Dispose ();

			return httpResult.IsSuccess ();
		}
	}
}
