/*

	Copyright © 2024 framebunker ApS
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
	associated documentation files (the “Software”), to deal in the Software without restriction, including
	without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
	the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial
	portions of the Software.
	
	THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
	LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
	NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
	
*/

namespace Matrix;

partial class API
{
	partial class Events
	{
		/// <summary>
		/// The timeout, in seconds, sent to the server for the "typing" ping 
		/// </summary>
		public const int kTypingTimeoutSeconds = 20;
		
		/// <summary>
		/// The frequency, in seconds between each one, at which "typing" pings should be sent for a steady state  
		/// </summary>
		public const int kTypingPingFrequencySeconds = 15;
		
		
		private const int kTypingPingTimeoutSeconds = 2;
		
		
		/// <summary>
		/// Outgoing room events, like messages
		/// </summary>
		public static class Send
		{
			// ReSharper disable InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
			#pragma warning disable CS0649
			private readonly record struct MessageResponseBody (string event_id);
			#pragma warning restore CS0649
			// ReSharper restore InconsistentNaming, IdentifierTypo, StringLiteralTypo, NotAccessedPositionalProperty.Local, UnusedType.Local
			
			
			/// <summary>
			/// Try to post a text message to a room
			/// </summary>
			/// <param name="server">The base URI of the Matrix server</param>
			/// <param name="accessToken">Token representing the active session</param>
			/// <param name="transactionID">Unique client-side transaction ID, to avoid duplicate processing</param>
			/// <param name="roomID">The room to which the message should be posted</param>
			/// <param name="body">Raw text body of the message</param>
			/// <returns>A boolean indicating success and the identifier for the resulting message event</returns>
			public static async Task<(bool success, string eventID)> TryMessageAsync (
				Uri server,
				string accessToken,
				string transactionID,
				string roomID,
				string body
			)
			{
				// Build request
			
				Uri request = new (server, $"{kRequestBase}/rooms/{roomID}/send/m.room.message/{transactionID}");

				// Run

				(HTTPResult result, MessageResponseBody response) =
					await TryHTTPPutAsync<EventContentMessageUnformattedText, MessageResponseBody> (
						request,
						new EventContentMessageUnformattedText (
							msgtype: "m.text",
							body: body
						),
						accessToken
					);

				return result.IsSuccess ()
					? (true, response.event_id)
					: (false, "");
			}


			/// <summary>
			/// Try to ping that a member is (still) typing in a room
			/// </summary>
			/// <param name="server">The base URI of the Matrix server</param>
			/// <param name="accessToken">Token representing the active session</param>
			/// <param name="roomID">The room in which the typing is happening</param>
			/// <param name="userID">The member of the room doing the typing</param>
			/// <returns>A boolean indicating success</returns>
			/// <remarks>The typing state times out in <see cref="Events.kTypingTimeoutSeconds"/>, meaning follow-up
			/// pings should be sent at <see cref="Events.kTypingPingFrequencySeconds"/></remarks>
			public static async Task<bool> TryTypingPing (
				Uri server,
				string accessToken,
				string roomID,
				string userID
			) => await TryTyping (
				server,
				accessToken,
				roomID,
				userID,
				new EventContentTypingSet (kTypingTimeoutSeconds * 1000)
			);


			/// <summary>
			/// Try to set that a member is no longer typing in a room
			/// </summary>
			/// <param name="server">The base URI of the Matrix server</param>
			/// <param name="accessToken">Token representing the active session</param>
			/// <param name="roomID">The room in which the typing is no longer happening</param>
			/// <param name="userID">The member of the room no longer doing the typing</param>
			/// <returns>A boolean indicating success</returns>
			/// <remarks>This can be sent when typing stops before the last <see cref="TryTypingPing"/> times out, but
			/// can also just be omitted - relying on the timeout in stead</remarks>
			public static async Task<bool> TryTypingUnset (
				Uri server,
				string accessToken,
				string roomID,
				string userID
			) => await TryTyping (server, accessToken, roomID, userID, new EventContentTypingUnset ());


			private static async Task<bool> TryTyping<TContent> (
				Uri server,
				string accessToken,
				string roomID,
				string userID,
				TContent content
			)
			where TContent : struct
			{
				// Build request
			
				Uri request = new (server, $"{kRequestBase}/rooms/{roomID}/typing/{userID}");
				
				// Run
				
				return (await TryHTTPPutAsync (
					request,
					content,
					accessToken,
					TimeSpan.FromSeconds (kTypingPingTimeoutSeconds)
				)).IsSuccess ();
			}
		}
	}	
}
